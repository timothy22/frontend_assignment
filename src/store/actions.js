import * as action from "./actionTypes";
import axios from "../axios";
import _ from "lodash";

export const stateChanger = (name, value, id, isCustom) => {
  return {
    type: action.STATE_CHANGE,
    data: { name: name, value: value, id: id, isCustom: isCustom },
  };
};

export const resetForm = () => {
  return {
    type: action.RESET_FORM,
  };
};

export const postHandler = () => {
  return (dispatch, getState) => {
    let number = getState().phone.trim().replace(/\s/g, "");
    if (getState().svkPrefix) {
      number = "+421" + number;
    } else {
      number = "+420" + number;
    }
    let data = {
      firstName: _.capitalize(getState().name),
      lastName: _.capitalize(getState().surname),
      email: getState().email,
      phone: number,
      value: getState().amount,
      shelterID: +getState().shelterID,
    };
    axios
      .post("/contribute", data)
      .then(response => {
        dispatch(resetForm());
      })
      .catch(error => {
        console.log(error);
      });
  };
};
