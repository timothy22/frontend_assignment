import * as actions from "./actionTypes";
import _ from "lodash";

const defaultState = {
  page: 1,
  receiverFoundation: true,
  shelter: "",
  amount: "",
  isCustom: false,
  name: "",
  surname: "",
  email: "",
  phone: "",
  gdpr: false,
  errorMessage: "",
  svkPrefix: true,
  shelterID: null,
};

const initialState = {
  ...defaultState,
  amountsList: ["5", "10", "20", "30", "50", "100"],
  detailInputsList: [
    {
      name: "name",
      placeholder: "Meno",
      max: "20",
    },
    {
      name: "surname",
      placeholder: "Priezvisko",
      max: "20",
    },
    {
      name: "email",
      placeholder: "E-mailová adresa",
      max: "40",
    },
  ],
};

const resetForm = state => {
  return {
    ...state,
    ...defaultState,
  };
};

const stateChanger = (state, action) => {
  if (action.data.name === "name" || action.data.name === "surname") {
    action.data.value = _.capitalize(action.data.value.toLowerCase());
  }
  if (action.data.name === "phone") {
    action.data.value = action.data.value.trim();
  }
  if (action.data.name === "email") {
    action.data.value = action.data.value.trim().toLowerCase();
  }
  if (action.data.name === "svkPrefix") {
    let newPrefix = !state.svkPrefix;
    return { ...state, svkPrefix: newPrefix };
  }
  if (action.data.name === "shelter") {
    return { ...state, shelter: action.data.value, shelterID: action.data.id };
  }
  if (action.data.name === "amount") {
    return {
      ...state,
      amount: action.data.value.trim().replace(/\s/g, "sd"),
      isCustom: action.data.isCustom,
    };
  }
  return { ...state, [action.data.name]: action.data.value };
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.STATE_CHANGE:
      return stateChanger(state, action);
    case actions.RESET_FORM:
      return resetForm(state);
    default:
      return state;
  }
};

export default rootReducer;
