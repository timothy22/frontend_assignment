import React from "react";
import classes from "./Indicator.module.css";
import { connect } from "react-redux";

const Indicator = props => {
  let indicatorClasses = [classes.Indicator, classes.Active];
  let indicator2Classes = [classes.Indicator];
  let indicator3Classes = [classes.Indicator];
  if (props.page === 2) {
    indicatorClasses.pop();
    indicator2Classes.push(classes.Active);
  } else if (props.page === 3) {
    indicatorClasses.pop();
    indicator3Classes.push(classes.Active);
  }
  return (
    <div className={classes.IndicatorsWrapper}>
      <div className={indicatorClasses.join(" ")}></div>
      <div className={indicator2Classes.join(" ")}></div>
      <div className={indicator3Classes.join(" ")}></div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    page: state.page,
  };
};

export default connect(mapStateToProps)(Indicator);
