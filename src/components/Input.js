import React from "react";
import classes from "./Input.module.css";

const Input = props => {
  return (
    <div className={classes.Input}>
      <input
        type="text"
        required
        maxLength={props.max}
        name={props.name}
        value={props.value}
        onChange={props.change}
      />
      <label>{props.placeholder}</label>
    </div>
  );
};

export default Input;
