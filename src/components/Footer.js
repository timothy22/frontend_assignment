import React from "react";
import classes from "./Footer.module.css";
import GoodBoyLogo from "../assets/images/logo-goodboy.svg";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Footer = props => {
  const { t } = useTranslation();
  return (
    <div className={classes.Footer}>
      <div className={classes.FooterColumn}>
        <img src={GoodBoyLogo} alt="Good Boy Logo" />
      </div>
      <div className={classes.FooterColumn}>
        <span className={classes.FooterHeading}>{t("footer text")}</span>
        <p>
          <Link to="/">{t("about us")}</Link>
          <Link to="/">{t("how to start")}</Link>
          <Link to="/">{t("contact us")}</Link>
        </p>
      </div>
      <div className={classes.FooterColumn}>
        <span className={classes.FooterHeading}>{t("footer text")}</span>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus in
          interdum ipsum, sit amet.
        </p>
      </div>
      <div className={classes.FooterColumn}>
        <span className={classes.FooterHeading}>{t("footer text")}</span>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus in
          interdum ipsum, sit amet.
        </p>
      </div>
    </div>
  );
};

export default Footer;
