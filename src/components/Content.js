import React from "react";
import classes from "./Content.module.css";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import MainPage from "../pages/MainPage";
import DetailsPage from "../pages/DetailsPage";
import CheckPage from "../pages/CheckPage";

const Content = props => {
  return (
    <div className={classes.Content}>
      {props.page === 1 ? <MainPage /> : null}
      {props.page === 2 ? <DetailsPage /> : null}
      {props.page === 3 ? <CheckPage /> : null}
      <Redirect to="/" />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    page: state.page,
  };
};

export default connect(mapStateToProps)(Content);
