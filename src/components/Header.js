import React from "react";
import classes from "./Header.module.css";
import fcbLogo from "../assets/images/facebook.svg";
import instaLogo from "../assets/images/instagram.svg";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Header = props => {
  const { t } = useTranslation();
  return (
    <div className={classes.Header}>
      <div className={classes.HeaderText}>
        {t("header text")}
        <div className={classes.Social}>
          <Link to="/">
            <img src={fcbLogo} alt="facebook" />
          </Link>

          <Link to="/">
            <img src={instaLogo} alt="instagram" />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Header;
