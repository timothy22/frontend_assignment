import React from "react";
import classes from "./Button.module.css";

const Button = props => {
  let buttonClasses = [];
  switch (props.type) {
    case "next":
      buttonClasses.push(classes.NextButton);
      break;
    case "back":
      buttonClasses.push(classes.BackButton);
      break;
    default:
      buttonClasses.push(classes.NextButton);
  }
  return (
    <button className={buttonClasses} onClick={props.click}>
      {props.children}
    </button>
  );
};

export default Button;
