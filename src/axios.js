import axios from "axios";

const instance = axios.create({
  baseURL: "https://frontend-assignment-api.goodrequest.com/api/v1/shelters",
});

export default instance;
