import React, { useState, useEffect } from "react";
import classes from "./MainPage.module.css";
import { connect } from "react-redux";
import * as actionCreators from "../store/actions";
import Indicator from "../components/Indicator";
import axios from "../axios";
import ErrorMessage from "../components/ErrorMessage";
import Button from "../components/Button";
import { useTranslation } from "react-i18next";

const MainPage = props => {
  const { t } = useTranslation();
  const [shelters, setShelters] = useState([]);

  const loadShelters = () => {
    axios
      .get()
      .then(response => {
        setShelters(response.data.shelters);
      })
      .catch(error => {
        console.log(error);
      });
  };

  useEffect(loadShelters, []);

  const valueHandler = (name, value, isCustom) => {
    let custom = false;
    if (isCustom) {
      custom = true;
    }
    props.stateChanger(name, value, null, custom);
  };

  const checkValidation = () => {
    if (!props.receiverFoundation && !props.shelter) {
      props.stateChanger("errorMessage", t("error shelter not selected"));
      return;
    }
    if (!props.amount || isNaN(props.amount)) {
      props.stateChanger("errorMessage", t("error amount not selected"));
      return;
    }
    props.stateChanger("errorMessage", "");
    props.stateChanger("page", 2);
  };

  let TargetBoxLeftClasses = [classes.TargetBoxLeft];
  let TargetBoxRightClasses = [classes.TargetBoxRight, classes.TargetBoxActive];
  if (!props.receiverFoundation) {
    TargetBoxRightClasses.pop();
    TargetBoxLeftClasses.push(classes.TargetBoxActive);
  }

  let optionsList = [];
  if (shelters) {
    shelters.forEach(shelter => {
      optionsList.push(
        <option
          className={classes.Option}
          data-tag={shelter.id}
          key={shelter.id}
        >
          {shelter.name}
        </option>
      );
    });
  }

  let buttonsList = [];
  if (props.amountsList) {
    props.amountsList.forEach(amount => {
      buttonsList.push(
        <button
          key={amount}
          className={[
            classes.Button,
            props.amount === amount && !props.isCustom
              ? classes.ButtonActive
              : null,
          ].join(" ")}
          onClick={
            props.amount === amount && !props.isCustom
              ? null
              : () => valueHandler("amount", amount)
          }
        >
          {amount} €
        </button>
      );
    });
  }

  return (
    <div className={classes.MainPage}>
      <Indicator />
      <h1>{t("choose support type")}</h1>
      <div className={classes.TargetBox}>
        <div
          onClick={
            props.receiverFoundation
              ? () => props.stateChanger("receiverFoundation", false)
              : null
          }
          className={TargetBoxLeftClasses.join(" ")}
        >
          <div
            className={
              props.receiverFoundation
                ? classes.LeftIcon
                : classes.LeftIconActive
            }
          ></div>
          {t("one shelter")}
        </div>
        <div
          onClick={
            props.receiverFoundation
              ? null
              : () => props.stateChanger("receiverFoundation", true)
          }
          className={TargetBoxRightClasses.join(" ")}
        >
          <div
            className={
              props.receiverFoundation
                ? classes.RightIconActive
                : classes.RightIcon
            }
          ></div>
          {t("whole foundation")}
        </div>
      </div>
      <div className={classes.SelectWrapper}>
        <div className={classes.Heading}>
          <div>{t("preferred shelter")}</div>
          <div className={classes.SelectInfo}>
            {props.receiverFoundation
              ? t("value not required")
              : t("value required")}
          </div>
        </div>
        <select
          name="shelter"
          className={classes.Select}
          onChange={e =>
            props.stateChanger(
              e.target.name,
              e.target.value,
              e.target.selectedOptions[0].getAttribute("data-tag")
            )
          }
          value={props.shelter}
        >
          <option disabled value="">
            {t("choose shelter")}
          </option>
          {optionsList}
        </select>
        <div className={classes.SelectIcon}></div>
      </div>

      <div className={classes.Heading}>{t("my amount")}</div>
      <div className={classes.ButtonsWrapper}>
        {buttonsList}
        <div className={classes.InputWrapper}>
          <input
            type="text"
            maxLength="5"
            className={classes.Input}
            name="amount"
            value={props.isCustom ? props.amount : ""}
            onChange={e => valueHandler("amount", e.target.value, true)}
          />
          €
        </div>
      </div>
      {props.errorMessage ? (
        <ErrorMessage>{props.errorMessage}</ErrorMessage>
      ) : null}
      <Button type="next" click={checkValidation}>
        {t("button continue")}
      </Button>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    page: state.page,
    receiverFoundation: state.receiverFoundation,
    shelter: state.shelter,
    amount: state.amount,
    isCustom: state.isCustom,
    errorMessage: state.errorMessage,
    amountsList: state.amountsList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    stateChanger: (name, value, id, isCustom) =>
      dispatch(actionCreators.stateChanger(name, value, id, isCustom)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
