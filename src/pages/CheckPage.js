import React from "react";
import classes from "./CheckPage.module.css";
import { connect } from "react-redux";
import * as actionCreators from "../store/actions";
import Indicator from "../components/Indicator";
import ErrorMessage from "../components/ErrorMessage";
import Button from "../components/Button";
import { useTranslation } from "react-i18next";

const CheckPage = props => {
  const { t } = useTranslation();

  const checkValidation = () => {
    if (!props.state.gdpr) {
      props.stateChanger("errorMessage", t("error you have to agree"));
      return;
    }
    props.stateChanger("errorMessage", "");
    postForm();
  };

  const postForm = () => {
    props.postHandler();
    alert(t("form posted"));
  };

  const goBack = () => {
    props.stateChanger("errorMessage", "");
    props.stateChanger("page", 2);
  };

  let num = props.state.phone;
  num = num.slice(0, 3) + " " + num.slice(3, 6) + " " + num.slice(6);

  let infoList = [];
  if (props.state.detailInputsList) {
    props.state.detailInputsList.forEach(input => {
      if (input.name === "name") {
        return;
      } else if (input.name === "surname") {
        infoList.push(
          <div key={input.name}>
            <div className={classes.Heading}>{t("name and surname")}</div>
            <div className={classes.Text}>
              {props.state.name + " " + props.state.surname}
            </div>
          </div>
        );
      } else {
        infoList.push(
          <div key={input.name}>
            <div className={classes.Heading}>{input.placeholder}</div>
            <div className={classes.Text}>{props.state[input.name]}</div>
          </div>
        );
      }
    });
  }

  return (
    <div className={classes.CheckPage}>
      <Indicator />
      <h1>{t("check details")}</h1>
      <div className={classes.Heading}>{t("my support type")}</div>
      <div className={classes.Text}>
        {props.state.receiverFoundation
          ? t("whole foundation")
          : t("one shelter")}
      </div>
      {props.state.shelter ? (
        <div className={classes.Heading}>{t("preferred shelter")}</div>
      ) : null}
      {props.state.shelter ? (
        <div className={classes.Text}>{props.state.shelter}</div>
      ) : null}
      <div className={classes.Heading}>{t("my amount")}</div>
      <div className={classes.Text}>{props.state.amount} €</div>
      {infoList}
      <div className={classes.Heading}>{t("phone number")}</div>
      <div className={classes.Text}>
        {props.state.svkPrefix ? "+421 " : "+420 "}
        {num}
      </div>
      <label className={classes.container}>
        <input
          type="checkbox"
          name="gdpr"
          id="gdpr"
          onChange={e => props.stateChanger(e.target.name, e.target.checked)}
        />
        <span className={classes.checkmark}></span>
        {t("gdpr agreement")}
      </label>
      {props.state.errorMessage ? (
        <ErrorMessage>{props.state.errorMessage}</ErrorMessage>
      ) : null}
      <div className={classes.ButtonsWrapper}>
        <Button type="back" click={goBack}>
          {t("button back")}
        </Button>
        <Button type="next" click={checkValidation}>
          {t("button send")}
        </Button>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    state: state,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    stateChanger: (name, value) =>
      dispatch(actionCreators.stateChanger(name, value)),
    postHandler: () => dispatch(actionCreators.postHandler()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckPage);
