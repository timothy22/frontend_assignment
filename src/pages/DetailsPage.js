import React from "react";
import classes from "./DetailsPage.module.css";
import { connect } from "react-redux";
import * as actionCreators from "../store/actions";
import Indicator from "../components/Indicator";
import ErrorMessage from "../components/ErrorMessage";
import Button from "../components/Button";
import Input from "../components/Input";
import { useTranslation } from "react-i18next";

const DetailsPage = props => {
  const { t } = useTranslation();
  const checkValidation = () => {
    let name = props.state.name.trim();
    let surname = props.state.surname.trim();
    if (!name) {
      props.stateChanger("errorMessage", t("error empty name"));
      return;
    }
    if (name.length < 2) {
      props.stateChanger("errorMessage", t("error short name"));
      return;
    }
    if (!surname) {
      props.stateChanger("errorMessage", t("error empty surname"));
      return;
    }
    if (surname.length < 2) {
      props.stateChanger("errorMessage", t("error short surname"));
      return;
    }
    if (!props.state.email) {
      props.stateChanger("errorMessage", t("error empty email"));
      return;
    }
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(String(props.state.email).toLowerCase())) {
      props.stateChanger("errorMessage", t("error wrong email format"));
      return;
    }
    if (!props.state.phone) {
      props.stateChanger("errorMessage", t("error empty phone"));
      return;
    }
    if (/^\+421|\+420|00421|00420/.test(props.state.phone)) {
      props.stateChanger("errorMessage", t("error set phone without prefix"));
      return;
    }
    if (isNaN(props.state.phone)) {
      props.stateChanger("errorMessage", t("error wrong phone format"));
      return;
    }
    props.stateChanger("errorMessage", "");
    props.stateChanger("page", 3);
  };

  const goBack = () => {
    props.stateChanger("errorMessage", "");
    props.stateChanger("page", 1);
  };

  let inputsList = [];
  if (props.state.detailInputsList) {
    props.state.detailInputsList.forEach(input => {
      inputsList.push(
        <Input
          key={input.name}
          name={input.name}
          max={input.max}
          placeholder={input.placeholder}
          change={e => props.stateChanger(e.target.name, e.target.value)}
          value={props.state[input.name]}
        />
      );
    });
  }

  return (
    <div className={classes.DetailsPage}>
      <Indicator />
      <h1>{t("need some info")}</h1>
      <div className={classes.Heading}>{t("about you")}</div>
      {inputsList}
      <div className={classes.PhoneWrapper}>
        <div className={classes.PhoneHeading}>{t("phone number")}</div>
        <div className={classes.PhoneContent}>
          <div
            className={classes.FlagWrapper}
            onClick={() => props.stateChanger("svkPrefix")}
          >
            {props.state.svkPrefix ? (
              <div className={classes.svkFlag}></div>
            ) : (
              <div className={classes.czeFlag}></div>
            )}
            <div className={classes.PhonePrefix}>
              {props.state.svkPrefix ? "+421" : "+420"}
            </div>
            <div className={classes.SelectIcon}></div>
          </div>

          <input
            className={classes.Input}
            name="phone"
            maxLength="12"
            onChange={e => props.stateChanger(e.target.name, e.target.value)}
            value={props.state.phone}
          />
        </div>
      </div>
      {props.state.errorMessage ? (
        <ErrorMessage>{props.state.errorMessage}</ErrorMessage>
      ) : null}
      <div className={classes.ButtonsWrapper}>
        <Button type="back" click={goBack}>
          {t("button back")}
        </Button>
        <Button type="next" click={checkValidation}>
          {t("button continue")}
        </Button>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    state: state,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    stateChanger: (name, value) =>
      dispatch(actionCreators.stateChanger(name, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPage);
