import i18n from "i18next";
import { initReactI18next } from "react-i18next";

const resources = {
  sk: {
    translation: {
      /*HEADER & FOOTER*/
      "header text": "Nadácia Good Boy",
      "footer text": "Nadácia Good boy",
      "about us": "O projekte",
      "how to start": "Ako na to",
      "contact us": "Kontakt",
      /*ERRORS*/
      "error shelter not selected":
        "Musíš vybrať ktorému útulku chceš venovať príspevok",
      "error amount not selected":
        "Musíš zadať číselný údaj akou sumou chceš prispieť",
      "error empty name": "Musíš vyplniť meno",
      "error short name": "Meno musí mať aspoň 2 znaky",
      "error empty surname": "Musíš vyplniť priezvisko",
      "error short surname": "Priezvisko musí mať aspoň 2 znaky",
      "error empty email": "Musíš vyplniť e-mailovú adresu",
      "error wrong email format": "E-mail nemá správny formát",
      "error empty phone": "Musíš vyplniť telefónne číslo",
      "error set phone without prefix": "Telefónne číslo zadávaj bez predvoľby",
      "error wrong phone format": "Telefónne číslo nie je v správnom formáte",
      "error you have to agree":
        "Musíš súhlasiť so spracovaním osobných údajov",
      /* ALERT */
      "form posted": "Formulár bol úspešne odoslaný",
      /*BUTTONS*/
      "button back": "Späť",
      "button continue": "Pokračovať",
      "button send": "Odoslať",
      /*MAIN PAGE*/
      "choose support type": "Vyberte si možnosť, ako chcete pomôcť",
      "one shelter": "Chcem finančne prispieť konkrétnemu útulku",
      "whole foundation": "Chcem finančne prispieť celej nadácii",
      "preferred shelter": "Najviac mi záleží na útulku",
      "value not required": "Nepovinné",
      "value required": "Povinné",
      "choose shelter": "Vyberte útulok zo zoznamu",
      "my amount": "Suma, ktorou chcem prispieť",
      /*DETAILS PAGE*/
      "need some info": "Potrebujeme od Vás zopár informácií",
      "about you": "O vás",
      "phone number": "Telefónne číslo",
      /*CHECK PAGE*/
      "check details": "Skontrolujte si zadané údaje",
      "gdpr agreement": "Súhlasím so spracovaním mojich osobných údajov",
      "my support type": "Akou formou chcem pomôcť",
      "name and surname": "Meno a priezvisko",
    },
  },
  //   en: {
  //     translation: {
  //       "Welcome to React": "Bienvenue à React et react-i18next",
  //     },
  //   },
};

i18n.use(initReactI18next).init({
  resources,
  lng: "sk",

  keySeparator: false,

  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
